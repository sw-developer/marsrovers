/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mars;

import exception.RoverException;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author daniel
 */
public class RoversTest {
    
    public RoversTest() {
    }

    @Test
    public void MoveToRoverTest1() {
        try {
            Rover rover = new Rover(new Position(1,2,'N'), 5, 5);
            rover.moveRoverTo("LM");
            Position result = new Position(0, 2, 'W');
            assertEquals(result, rover.getPosition());
        } catch (RoverException ex) {
            fail(ex.getMessage());
        }
    }
    
    @Test
    public void MoveToRoverTest2() {
        try {
            Rover rover = new Rover(new Position(1,2,'N'), 5, 5);
            rover.moveRoverTo("LMLMLMLMM");
            Position result = new Position(1, 3, 'N');
            assertEquals(result, rover.getPosition());
        } catch (RoverException ex) {
            fail(ex.getMessage());
        }
    }
    
    @Test
    public void MoveToRoverTest3() {
        try {
            Rover rover = new Rover(new Position(3,3,'E'), 5, 5);
            rover.moveRoverTo("MMRMMRMRRM");
            Position result = new Position(5, 1, 'E');
            assertEquals(result, rover.getPosition());
        } catch (RoverException ex) {
            fail(ex.getMessage());
        }
    }
    
    @Test
    public void MoveToRoverTest4() {
        try {
            Rover rover = new Rover(new Position(2,1,'E'), 6, 4);
            rover.moveRoverTo("MLMLMLMLMRMLMMLMRMRR");
            Position result = new Position(6, 1, 'W');
            assertEquals(result, rover.getPosition());
        } catch (RoverException ex) {
            fail(ex.getMessage());
        }
    }
    
    @Test
    public void MoveToRoverFailMovement() {
        Rover rover = new Rover(new Position(2,1,'E'), 6, 4);
        try {
            rover.moveRoverTo("MLMLMLMLMRMM");
            fail();
        } catch (RoverException ex) {
            Position result = new Position(3, 0, 'S');
            assertEquals(ex.getMessage(), result, rover.getPosition());
        }
    }
    
    @Test
    public void MoveToRoverOneMovement() {
        try {
            Rover rover = new Rover(new Position(0,0,'E'), 0, 0);
            rover.moveRoverTo("LLLR");
            Position result = new Position(0, 0, 'W');
            assertEquals(result, rover.getPosition());
        } catch (RoverException ex) {
            fail(ex.getMessage());
        }
    }
    
    @Test
    public void MoveToRoverFailOneMovement() {
        Rover rover = new Rover(new Position(0,0,'E'), 0, 0);
        try {
            rover.moveRoverTo("LM");
            fail();
        } catch (RoverException ex) {
            Position result = new Position(0, 0, 'N');
            assertEquals(ex.getMessage(), result, rover.getPosition());
        }
    }
}
