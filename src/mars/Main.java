/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mars;

import com.sun.xml.internal.ws.api.pipe.NextAction;
import java.util.Scanner;

/**
 *
 * @author daniel
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int maxX = scanner.nextInt();
        int maxY = scanner.nextInt();

        //the eof input windows = ctrl+z; xNix = ctrl+d
        while (scanner.hasNextLine()){
            int x = scanner.nextInt();
            int y = scanner.nextInt();
            char head = scanner.next().charAt(0);
            String instructions = scanner.next();
            
            Rover rover = new Rover(x, y, head, maxX, maxY);
            try {
                rover.moveRoverTo(instructions);
                System.out.println(rover.getPosition());
            } catch (Exception e) {
                System.err.println(e);
            }
        }
        scanner.close();
        System.out.println("==========");
    }
}
