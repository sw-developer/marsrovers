/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mars;

import exception.InvalidHeadException;
import exception.InvalidPositionException;
import exception.RoverException;

/**
 *
 * @author daniel
 */
public class Rover {
    
    public enum HeadDirection {

        N, S, E, W
    };

    public enum HeadMovement {

        L, R
    };
    
    private static final char movements[][] = {{'W', 'E', 'N', 'S'},
                                                 {'E', 'W', 'S', 'N'}};
    
    private Position position;
    private int maxX;
    private int maxY;

    public Rover(Position position, int maxX, int maxY) {
        this.position = new Position(position);
        this.maxX = maxX;
        this.maxY = maxY;
    }
    
    public Rover(int firstX, int firstY, char firstHead, int maxX, int maxY) {
        this.position = new Position(firstX, firstY, String.valueOf(firstHead).toUpperCase().charAt(0));
        this.maxX = maxX;
        this.maxY = maxY;
    }
    
    public void moveRoverTo(String instructions) throws RoverException {
        for (char instruction : instructions.toUpperCase().toCharArray()) {
            this.moveRover(instruction);
        }
    }

    public void moveRover(char instruction) throws RoverException {
        if (instruction == 'M') {
            movePosition();
        } else {
            moveHead(instruction);
        }
    }

    private void moveHead(char istruction) throws InvalidHeadException {
        int movement = HeadMovement.valueOf(String.valueOf(istruction)).ordinal();
        int direction = HeadDirection.valueOf(String.valueOf(this.position.getHead())).ordinal();
        this.position.setHead(movements[movement][direction]);
    }

    private void movePosition() throws InvalidPositionException {
        int x = this.position.getX();
        int y = this.position.getY();

        switch (this.position.getHead()) {
            case 'N':
                y += 1;
                break;
            case 'S':
                y -= 1;
                break;
            case 'E':
                x += 1;
                break;
            case 'W':
                x -= 1;
                break;
            default:
                x = -1;
                y = -1;
                break;
        }
        if (x < 0 || y < 0 || x > maxX || y > maxY) {
            throw new InvalidPositionException();
        }
        this.position.setX(x);
        this.position.setY(y);
    }
    
    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }
    
    @Override
    public String toString() {
        return "Rover{" + "position=" + position + ", maxX=" + maxX + ", maxY=" + maxY + '}';
    }
}
