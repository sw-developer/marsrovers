/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mars;

/**
 *
 * @author daniel
 */
public class Position {

    private int x;
    private int y;
    private char head;
    
    public Position(){
        this.x = 0;
        this.y = 0;
        this.head = 'N';
    }

    public Position(int x, int y, char head) {
        this.x = x;
        this.y = y;
        this.head = head;
    }
    
    public Position(Position position){
        this.x = position.x;
        this.y = position.y;
        this.head = position.head;
    }
    
    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public char getHead() {
        return head;
    }

    public void setHead(char head) {
        this.head = head;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Position other = (Position) obj;
        if (this.x != other.x) {
            return false;
        }
        if (this.y != other.y) {
            return false;
        }
        if (this.head != other.head) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return x + " " + y + " " + head;
    }
}
