/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package exception;

/**
 *
 * @author daniel
 */
public class RoverException extends Exception{
    
    public RoverException(){
        super("Operaccion invalida.");
    }
    
    public RoverException(String message){
        super(message);
    }
    
}
